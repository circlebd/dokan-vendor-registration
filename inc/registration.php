<?php

    class DokanVendorRegistration
    {
        public function init(){
            add_action( 'admin_footer', array( $this, 'register_new_vendor' ) );
            add_action( 'wp_ajax_register_new_vendor_ajax', array( &$this, 'register_new_vendor_ajax' ) );
        }

        public static function register_new_vendor(){
            $page = $_GET[ 'page' ];
            $form_id = $_GET[ 'id' ];
            $entry_id = $_GET[ 'lid' ];
            $form = GFAPI::get_form( $form_id );
            $already_registered = gform_get_meta( $entry_id, 'vendor_registered' );
            $registered_id = $already_registered ? gform_get_meta( $entry_id, 'vendor_id' ): false;
            
            if( $page === 'gf_entries' && $form['cssClass'] === 'homechef-registration' ):
                if( $registered_id ):
                    $user_link = get_edit_user_link( $registered_id );
?>
                    <script type="text/javascript">
                        jQuery( '#submitdiv #minor-publishing' ).append( '<a href="<?php echo $user_link; ?>" target="_blank">Vendor profile</a>' );
                    </script>
<?php
                else:
?>
                    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
                    <script type="text/javascript">
                        var page = '<?php echo $page; ?>';
                        var entry_id = '<?php echo $entry_id; ?>';
                        var nonce = '<?php echo wp_create_nonce( 'vendor_registration_nonce' ); ?>';
                        var ajaxurl = '<?php echo admin_url( 'admin-ajax.php' ); ?>';
                        jQuery( document ).ready( function( $ ){
                            $( '<input id="register_vendor_button" class="button button-large" type="submit" style="margin-left: 5px" value="Register" name="save">' ).insertAfter( '#submitdiv #gform_edit_button' );
                            $( '#register_vendor_button' ).on( 'click', function( e ){
                                e.preventDefault();
                                $.ajax({
                                    type : "post",
                                    dataType : "json",
                                    url : ajaxurl,
                                    data : { action: "register_new_vendor_ajax", entry_id : entry_id, nonce: nonce },
                                    success: function( response ) {
                                        console.log( response.entry );  
                                        if( response.success ) {
                                            console.log( response.entry );                                    
                                            swal("Succesfully Registered!");
                                        }
                                        else {                                        
                                            swal( 'Sorry !! You cannot register right now!! Please call at 01710 50 59 25 !!' );
                                        }
                                    }
                                });
                            });
                        });
                    </script>
<?php
                endif;
            endif;
        }

        public static function register_new_vendor_ajax(){
            if ( !wp_verify_nonce( $_REQUEST['nonce'], "vendor_registration_nonce" )) {
                exit( 'Sorry, I am secured !!!' );
            }
            $success = false;
            $entry_id = isset( $_REQUEST[ 'entry_id' ] ) && !empty( $_REQUEST[ 'entry_id' ] ) ? $_REQUEST[ 'entry_id' ] : false;
            $already_registered = gform_get_meta( $entry_id, 'vendor_registered' );
            if( $entry_id && ! $already_registered ){
                $entry = GFAPI::get_entry( $entry_id );
                if( !empty( $entry ) ){
                    $user = SELF::register_as_user( $entry );
                    if ( $user && ! is_wp_error( $user ) ) $success = true;
                }                
            }
            echo json_encode( array(
                'entry' => $user,
                'success' => $success
            ));
            die();
        }

        public static function register_as_user( $entry ){
            $user_args = DokanVendorFunction::arrange_form_fields( $entry[ 'form_id' ], $entry );
            $user_id = false;
            if( !empty( $user_args ) ){
                $vendor_profile_setting = DokanVendorFunction::set_vendor_profile_settings( $user_args );        
                $x = 1;
                $y = 2;
                while( $x < $y ):
                    $z = $x < 2 ? '' : $x;
                    $user_name = !empty( $user_args[ 'vendor_name' ] ) ? DokanVendorFunction::slugify( $user_args[ 'vendor_name' ][ 'first_name' ].' '.$user_args[ 'vendor_name' ][ 'last_name' ].$z ) : '';
                    $user_id = username_exists( $user_name );
                    $x++;
                    $y++;
                    
                    if ( ! $user_id && false == email_exists( $user_email ) ) {
                        $random_password = wp_generate_password( 12, false );
                        $vendor_data = array(
                            'user_pass' => $random_password,
                            'first_name' => $user_args[ 'vendor_name' ][ 'first_name' ],
                            'last_name' => $user_args[ 'vendor_name' ][ 'last_name' ],
                            'user_login'    => $user_name,
                            'user_nicename' => DokanVendorFunction::slugify( $vendor_profile_setting['store_name'] ),
                            'user_email' => $user_args[ 'user_email' ],
                            //'user_email' => 'test4@email.com',
                            'role'  => 'seller',
                        );
                        $user_id = wp_insert_user( $vendor_data );
                        if( $user_id && ! is_wp_error( $user_id ) ) { 
                            $y--;
                            update_user_meta( $user_id, 'dokan_profile_settings', $vendor_profile_setting );
                            update_user_meta( $user_id, 'dokan_enable_selling', 'no' );
                            update_user_meta( $user_id, 'dokan_store_name', $vendor_profile_setting['store_name'] );
                            update_user_meta( $user_id, 'dokan_user_area', !empty( $user_args[ 'others_area' ] ) ? $user_args[ 'others_area' ] : $user_args[ 'vendor_area' ] );
                            
                            // update entry meta
                            gform_update_meta( $entry[ 'id' ], 'vendor_registered', true );
                            gform_update_meta( $entry[ 'id' ], 'vendor_id', $user_id );
                            
                            //Send notifications
                            DokanVendorNotifications::send_notifications( $user_args[ 'phone' ], $user_args[ 'user_email' ], $user_name, $random_password );
                        }
                    }
                endwhile;
                return $user_id;
            }
        }

    }

    $dokan_registration = new DokanVendorRegistration;
    $dokan_registration->init();