<?php

    class DokanVendorNotifications{

        public static function send_notifications( $phone = '', $user_email = '', $username = '', $password = '' )
        {
            //Send SMS
            //if( ! empty( $phone ) ) SELF::send_sms( $phone, $username, $password );
            //Send Email
            if( ! empty( $user_email ) ) SELF::send_email( $user_email, $username, $password );
        }

        public static function send_sms( $res_number, $username, $password ){
            $otp_username = '01628083283';
            $otp_password = 'KBWA4RH7';
            $res_number = trim( $res_number );
            $msg = "Welcome to Hungrypanda Bangladesh\r\n";
            $msg .= "Login Detail\r\n";
            $msg .= "https://hungrypandabd.com/my-account/\r\n";
            $msg .= "Username: ".$username."\r\n";
            $msg .= "Password: ".$password;
            $msg = urlencode( $msg );
            $smsresult = file_get_contents("http://66.45.237.70/api.php?username=$otp_username&password=$otp_password&number=$res_number&message=$msg");
            return $smsresult;
        }

        public static function send_email( $email_address, $username, $password ){
            $to = $email_address;
            $subject = 'Congratulations !! Welcome to Hungrypanda Bangladesh !!!';
            $headers = array();
            $headers[] = 'MIME-Version: 1.0';
            $headers[] = 'Content-Type: text/html; charset=iso-8859-1';
            $headers[] = 'From: Hungrypanda Bngladesh <no-reply@hungrypandabd.com>';
            //$headers[] = 'Bcc: hungrypandabd@gmail.com';

            $message = SELF::registration_email( $username, $password );

            return wp_mail( $to, $subject, $message, $headers );
        }

        public static function registration_email( $username, $password ){
            ob_start();
?>
            <!DOCTYPE html>
                <html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office" style="-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;margin: 0 auto !important;padding: 0 !important;height: 100% !important;width: 100% !important;background: #f1f1f1 !important;">
                <head style="-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                    <meta charset="utf-8" style="-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"> <!-- utf-8 works for most cases -->
                    <meta name="viewport" content="width=device-width" style="-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"> <!-- Forcing initial-scale shouldn't be necessary -->
                    <meta http-equiv="X-UA-Compatible" content="IE=edge" style="-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"> <!-- Use the latest (edge) version of IE rendering engine -->
                    <meta name="x-apple-disable-message-reformatting" style="-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">  <!-- Disable auto-scale in iOS 10 Mail entirely -->
                    <title style="-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"></title> <!-- The title tag shows in email notifications, like Android 4.4. -->

                    <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,400i,700,700i" rel="stylesheet" style="-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                </head>

                <body width="100%" style="margin: 0 auto !important;padding: 0 !important;mso-line-height-rule: exactly;background-color: #222222;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;font-family: 'Montserrat', sans-serif;font-weight: 400;font-size: 15px;line-height: 1.8;color: rgba(0,0,0,.4);height: 100% !important;width: 100% !important;background: #f1f1f1 !important;">
                    <center style="width: 100%;background-color: #f1f1f1;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                    <div style="display: none;font-size: 1px;max-height: 0px;max-width: 0px;opacity: 0;overflow: hidden;mso-hide: all;font-family: sans-serif;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                    &zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;
                    </div>
                    <div style="max-width: 600px;margin: 0 auto;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" class="email-container">
                        <!-- BEGIN BODY -->
                    <table align="center" role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="margin: auto;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;mso-table-lspace: 0pt !important;mso-table-rspace: 0pt !important;border-spacing: 0 !important;border-collapse: collapse !important;table-layout: fixed !important;">
                        <tr style="-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                        <td class="bg_white logo" style="padding: 1em 2.5em;text-align: center;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background: #ffffff;mso-table-lspace: 0pt !important;mso-table-rspace: 0pt !important;">
                            <h1 style="-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;font-family: 'Playfair Display', serif;color: #000000;margin-top: 0;margin: 0;"><a href="https://hungrypandabd.com/" style="-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;text-decoration: none;color: #000;font-size: 20px;font-weight: 700;text-transform: uppercase;font-family: 'Montserrat', sans-serif;"><img width="200" src="https://www.hungrypandabd.com/wp-content/uploads/2020/05/cropped-hungrypanda-logo-web.png" style="-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;-ms-interpolation-mode: bicubic;"></a></h1>
                        </td>
                        </tr><!-- end tr -->
                                <tr style="-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                        <td valign="middle" class="hero" style="background-image: url( https://www.hungrypandabd.com/wp-content/uploads/2020/10/rsz_1big-07.jpg );background-size: cover;height: 400px;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;position: relative;mso-table-lspace: 0pt !important;mso-table-rspace: 0pt !important;">
                            <table style="-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;mso-table-lspace: 0pt !important;mso-table-rspace: 0pt !important;border-spacing: 0 !important;border-collapse: collapse !important;table-layout: fixed !important;margin: 0 auto !important;">
                                <tr style="-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                    <td style="-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;mso-table-lspace: 0pt !important;mso-table-rspace: 0pt !important;">
                                        <div class="text" style="padding: 0 3em;text-align: center;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: rgba(255,255,255,.8);">
                                            <h2 style="-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;font-family: 'Playfair Display', serif;color: #ffffff;margin-top: 0;font-size: 30px;margin-bottom: 0;">Online Homemade Food & Grocery</h2>
                                            <p style="-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">Hungrypanda is the best platform to sell your homemade food and grocery items to the people of your city.</p>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        </tr><!-- end tr -->
                        <tr style="-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                            <td class="bg_white" style="-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background: #ffffff;mso-table-lspace: 0pt !important;mso-table-rspace: 0pt !important;">
                                <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;mso-table-lspace: 0pt !important;mso-table-rspace: 0pt !important;border-spacing: 0 !important;border-collapse: collapse !important;table-layout: fixed !important;margin: 0 auto !important;">
                                <tr style="-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                    <td class="bg_dark email-section" style="text-align: center;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background: rgba(0,0,0,.8);padding: 2.5em;mso-table-lspace: 0pt !important;mso-table-rspace: 0pt !important;">
                                        <div class="heading-section heading-section-white" style="-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: rgba(255,255,255,.8);">
                                            <span class="subheading" style="-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;display: inline-block;font-size: 13px;text-transform: uppercase;letter-spacing: 2px;color: rgba(255,255,255,.4);position: relative;margin-bottom: 20px !important;">Welcome</span>
                                        <h2 style="-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;font-family: line-height: 1;color: #ffffff;margin-top: 0;font-size: 28px;line-height: 1.4;padding-bottom: 0;">Welcome To Hungrypanda Bangladesh</h2>
                                        <p style="-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">Thank you for registering with us as a Homechef. Here is your login detail.</p>
                                    <p style="-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                    <ul class="user_info" style="-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;list-style: none;">
                                        <li style="-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">Login link: <a href="https://hungrypandabd.com/my-account/" target="_blank" style="-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;text-decoration: none;color: #f3a333;">Click here</a></li>
                                        <li style="-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">Username: <?php echo $username; ?></li>
                                        <li style="-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">Password: <?php echo $password; ?></li>
                                        <li style="-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">You can change your password anytime</li>
                                    </ul>
                                    </p>
                                </div>
                                    </td>
                                </tr><!-- end: tr -->
                                </table>

                            </td>
                            </tr><!-- end:tr -->
                    <!-- 1 Column Text + Button : END -->
                    </table>
                    <table align="center" role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="margin: auto;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;mso-table-lspace: 0pt !important;mso-table-rspace: 0pt !important;border-spacing: 0 !important;border-collapse: collapse !important;table-layout: fixed !important;">
                        <tr style="-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                        <td valign="middle" class="bg_black footer email-section" style="-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background: #000000;padding: 2.5em;color: rgba(255,255,255,.5);mso-table-lspace: 0pt !important;mso-table-rspace: 0pt !important;">
                            <table style="-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;mso-table-lspace: 0pt !important;mso-table-rspace: 0pt !important;border-spacing: 0 !important;border-collapse: collapse !important;table-layout: fixed !important;margin: 0 auto !important;">
                                <tr style="-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                <td valign="top" width="33.333%" style="padding-top: 20px;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;mso-table-lspace: 0pt !important;mso-table-rspace: 0pt !important;">
                                <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;mso-table-lspace: 0pt !important;mso-table-rspace: 0pt !important;border-spacing: 0 !important;border-collapse: collapse !important;table-layout: fixed !important;margin: 0 auto !important;">
                                    <tr style="-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                    <td style="text-align: left;padding-right: 10px;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;mso-table-lspace: 0pt !important;mso-table-rspace: 0pt !important;">
                                        <h3 class="heading" style="-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;font-family: 'Playfair Display', serif;color: #ffffff;margin-top: 0;font-size: 20px;">Hungrypanda</h3>
                                        <p style="-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">Hungrypanda Bangladesh is a common platform for buying and selling homemade food online</p>
                                    </td>
                                    </tr>
                                </table>
                                </td>
                                <td valign="top" width="33.333%" style="padding-top: 20px;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;mso-table-lspace: 0pt !important;mso-table-rspace: 0pt !important;">
                                <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;mso-table-lspace: 0pt !important;mso-table-rspace: 0pt !important;border-spacing: 0 !important;border-collapse: collapse !important;table-layout: fixed !important;margin: 0 auto !important;">
                                    <tr style="-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                    <td style="text-align: left;padding-left: 5px;padding-right: 5px;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;mso-table-lspace: 0pt !important;mso-table-rspace: 0pt !important;">
                                        <h3 class="heading" style="-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;font-family: 'Playfair Display', serif;color: #ffffff;margin-top: 0;font-size: 20px;">Contact Info</h3>
                                        <ul style="-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;margin: 0;padding: 0;">
                                                    <li style="-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;list-style: none;margin-bottom: 10px;"><span class="text" style="-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">41/1322, Polashpur, Kadomtoli, Dhaka – 1236</span></li>
                                                    <li style="-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;list-style: none;margin-bottom: 10px;"><span class="text" style="-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">+88 01624 319 625</span></li>
                                                </ul>
                                    </td>
                                    </tr>
                                </table>
                                </td>
                                <td valign="top" width="33.333%" style="padding-top: 20px;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;mso-table-lspace: 0pt !important;mso-table-rspace: 0pt !important;">
                                <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;mso-table-lspace: 0pt !important;mso-table-rspace: 0pt !important;border-spacing: 0 !important;border-collapse: collapse !important;table-layout: fixed !important;margin: 0 auto !important;">
                                    <tr style="-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                    <td style="text-align: left;padding-left: 10px;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;mso-table-lspace: 0pt !important;mso-table-rspace: 0pt !important;">
                                        <h3 class="heading" style="-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;font-family: 'Playfair Display', serif;color: #ffffff;margin-top: 0;font-size: 20px;">Useful Links</h3>
                                        <ul style="-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;margin: 0;padding: 0;">
                                                    <li style="-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;list-style: none;margin-bottom: 10px;"><a href="https://hungrypandabd.com" style="-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;text-decoration: none;color: rgba(255,255,255,1);">Home</a></li>
                                                    <li style="-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;list-style: none;margin-bottom: 10px;"><a href="https://hungrypandabd.com/shop/" style="-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;text-decoration: none;color: rgba(255,255,255,1);">Shop</a></li>
                                                    <li style="-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;list-style: none;margin-bottom: 10px;"><a href="https://hungrypandabd.com/privacy-policy/" style="-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;text-decoration: none;color: rgba(255,255,255,1);">Privacy Policy</a></li>
                                                    <li style="-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;list-style: none;margin-bottom: 10px;"><a href="https://hungrypandabd.com/terms-condition/" style="-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;text-decoration: none;color: rgba(255,255,255,1);">Terms & Condition</a></li>
                                                </ul>
                                    </td>
                                    </tr>
                                </table>
                                </td>
                            </tr>
                            </table>
                        </td>
                        </tr><!-- end: tr -->
                        <tr style="-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                            <td valign="middle" class="bg_black footer email-section" style="-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background: #000000;padding: 2.5em;color: rgba(255,255,255,.5);mso-table-lspace: 0pt !important;mso-table-rspace: 0pt !important;">
                                <table style="-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;mso-table-lspace: 0pt !important;mso-table-rspace: 0pt !important;border-spacing: 0 !important;border-collapse: collapse !important;table-layout: fixed !important;margin: 0 auto !important;">
                                <tr style="-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                <td valign="top" width="100%" style="-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;mso-table-lspace: 0pt !important;mso-table-rspace: 0pt !important;">
                                <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;mso-table-lspace: 0pt !important;mso-table-rspace: 0pt !important;border-spacing: 0 !important;border-collapse: collapse !important;table-layout: fixed !important;margin: 0 auto !important;">
                                    <tr style="-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                    <td style="text-align: left;padding-right: 10px;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;mso-table-lspace: 0pt !important;mso-table-rspace: 0pt !important;">
                                        <p style="-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">&copy; 2018 Hungrypanda Bangladesh. All Rights Reserved</p>
                                    </td>
                                    </tr>
                                </table>
                                </td>
                            </tr>
                            </table>
                            </td>
                        </tr>
                    </table>

                    </div>
                </center>
                </body>
                </html>
<?php
            $html = ob_get_contents();
            ob_end_clean();
            return $html;
        }
    }