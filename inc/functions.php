<?php

    class DokanVendorFunction
    {

        public static function set_vendor_profile_settings( $args ){

            $settings = array();

            if( !empty( $args ) ){
                $settings = array (
                    'store_name' => $args[ 'store_name' ],
                    'payment' => 
                    array (
                    'bank' => 
                    array (
                        'ac_name' => $args[ 'ac_name' ],
                        'ac_number' => $args[ 'ac_number' ],
                        'bank_name' => $args[ 'bank_name' ],
                        'bank_addr' => $args[ 'bank_addr' ],
                    ),
                    ),
                    'phone' => $args[ 'phone' ],
                    'show_email' => 'no',
                    'address' => 
                    array (
                    'street_1' => $args[ 'address' ][ 'street_1' ],
                    'street_2' => $args[ 'address' ][ 'street_2' ],
                    'city' => $args[ 'address' ][ 'city' ],
                    'zip' => $args[ 'address' ][ 'zip' ],
                    'country' => $args[ 'address' ][ 'country' ],
                    'state' => $args[ 'address' ][ 'state' ],
                    ),
                    'show_more_ptab' => 'yes',
                    'store_ppp' => '10',
                    'enable_tnc' => 'off',
                    'store_tnc' => '',
                    'show_min_order_discount' => 'no',
                    'dokan_store_time_enabled' => 'yes',
                );
            }
            return $settings;

        }

        public static function arrange_form_fields( $form_id, $entry ){
            $user_args = array();
            $form = GFAPI::get_form( $form_id );
            if( isset( $form['cssClass'] ) && $form['cssClass'] == 'homechef-registration' ):
                $user_meta = array(
                    'business_name' => 'store_name',
                    'fb_url' => 'fb_page',
                    'phone_number' => 'phone',
                    'email_address' => 'user_email',
                    'bkash_number' => 'bkash_number',
                    'ba_name' => 'ac_name',
                    'ba_number' => 'ac_number',
                    'bank_name' => 'bank_name',
                    'bank_address' => 'bank_addr',
                    'vendor_area' => 'vendor_area',
                    'others_area'   => 'others_area'
                );
                foreach ( $form['fields'] as $field ):
                    if( isset( $field['cssClass'] ) && !empty( $field['cssClass']  ) ):
                        if( strpos( $field[ 'cssClass' ], 'vendor_name' ) !== false ):
                            $user_args[ 'vendor_name' ][ 'first_name' ] = $entry[ $field['id'].'.3' ];
                            $user_args[ 'vendor_name' ][ 'last_name' ] = $entry[ $field['id'].'.6' ];
                        elseif( strpos( $field[ 'cssClass' ], 'vendor_address' ) !== false ):
                            $user_args[ 'address' ][ 'street_1' ] = $entry[ $field['id'].'.1' ];
                            $user_args[ 'address' ][ 'street_2' ] = $entry[ $field['id'].'.2' ];
                            $user_args[ 'address' ][ 'city' ] = $entry[ $field['id'].'.3' ];
                            $user_args[ 'address' ][ 'zip' ] = $entry[ $field['id'].'.5' ];
                            $user_args[ 'address' ][ 'country' ] = 'BD';
                            $user_args[ 'address' ][ 'state' ] = 'BD-13';
                        else:
                            foreach( $user_meta as $key=>$value ){
                                if( strpos( $field[ 'cssClass' ], $key ) !== false )
                                    $user_args[ $user_meta[ $key ] ] = $entry[ $field['id'] ];
                            }
                        endif;
                    endif;
                endforeach;
            endif;
            return $user_args;
        }

        // Slugify

        public static function slugify( $string )
        {
            if( !empty( $string ) ):
                return strtolower(trim(preg_replace('~[^0-9a-z]+~i', '_', html_entity_decode(preg_replace('~&([a-z]{1,2})(?:acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml);~i', '$1', htmlentities($string, ENT_QUOTES, 'UTF-8')), ENT_QUOTES, 'UTF-8')), '_'));
            else:
                return false;
            endif;
        }
    }


    //Some Extra Code


    //add_action( 'gform_after_submission', 'access_entry_via_field', 10, 2 );
    function access_entry_via_field( $entry, $form ) {
        if( isset( $form['cssClass'] ) && $form['cssClass'] == 'homechef-registration' ):
            $user_meta = array(
                'business_name' => 'store_name',
                'fb_url' => 'fb_page',
                'phone_number' => 'phone',
                'email_address' => 'user_email',
                'bkash_number' => 'bkash_number',
                'ba_name' => 'ac_name',
                'ba_number' => 'ac_number',
                'bank_name' => 'bank_name',
                'bank_address' => 'bank_addr',
            );
            $user_args = array();
            foreach ( $form['fields'] as $field ):
                if( isset( $field['cssClass'] ) && !empty( $field['cssClass']  ) ):
                    if( strpos( $field[ 'cssClass' ], 'vendor_name' ) !== false ):
                        $user_args[ 'vendor_name' ][ 'first_name' ] = $entry[ $field['id'].'.3' ];
                        $user_args[ 'vendor_name' ][ 'last_name' ] = $entry[ $field['id'].'.6' ];
                    elseif( strpos( $field[ 'cssClass' ], 'vendor_address' ) !== false ):
                        $user_args[ 'address' ][ 'street_1' ] = $entry[ $field['id'].'.1' ];
                        $user_args[ 'address' ][ 'street_2' ] = $entry[ $field['id'].'.2' ];
                        $user_args[ 'address' ][ 'city' ] = $entry[ $field['id'].'.3' ];
                        $user_args[ 'address' ][ 'zip' ] = $entry[ $field['id'].'.5' ];
                        $user_args[ 'address' ][ 'country' ] = 'BD';
                        $user_args[ 'address' ][ 'state' ] = 'BD-13';
                    else:
                        foreach( $user_meta as $key=>$value ){
                            if( strpos( $field[ 'cssClass' ], $key ) !== false )
                                $user_args[ $user_meta[ $key ] ] = $entry[ $field['id'] ];
                        }
                    endif;
                endif;
            endforeach;
        endif;
        if( !empty( $user_args ) ){
            $vendor_profile_setting = set_vendor_profile_settings( $user_args );        
            $x = 1;
            $y = 2;
            while( $x < $y ):
                $z = $x < 2 ? '' : $x;
                $user_name = !empty( $user_args[ 'vendor_name' ] ) ? slugify( $user_args[ 'vendor_name' ][ 'first_name' ].' '.$user_args[ 'vendor_name' ][ 'last_name' ].$z ) : '';
                $user_id = username_exists( $user_name );
                $x++;
                $y++;
                
                if ( ! $user_id && false == email_exists( $user_email ) ) {
                    $random_password = wp_generate_password( 12, false );
                    $vendor_data = array(
                        'user_pass' => $random_password,
                        'first_name' => $user_args[ 'vendor_name' ][ 'first_name' ],
                        'last_name' => $user_args[ 'vendor_name' ][ 'last_name' ],
                        'user_login'    => $user_name,
                        'user_nicename' => slugify( $vendor_profile_setting['store_name'] ),
                        //'user_email' => $user_args[ 'user_email' ],
                        'user_email' => 'test3@email.com',
                        'role'  => 'seller',
                    );
                    $user_id = wp_insert_user( $vendor_data );
                    if( $user_id ) { 
                        $y--;
                        update_user_meta( $user_id, 'dokan_profile_settings', $vendor_profile_setting );
                        update_user_meta( $user_id, 'dokan_enable_selling', 'no' );
                        update_user_meta( $user_id, 'dokan_store_name', $vendor_profile_setting['store_name'] );
                    }
                }
            endwhile;
        }
        echo '<pre>';
        //print_r( $user_args );
        //print_r( $entry );
        //print_r( $form );
        print_r( $user_id );
        echo '</pre>';
    }